
# coding: utf-8

# # Libraries import

# In[1]:

#import standard modules
import numpy as np
import pandas as pd
get_ipython().magic('matplotlib inline')
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.cross_validation import train_test_split
from sklearn.metrics import roc_curve
import matplotlib.pyplot as plt
from sklearn.metrics import precision_recall_curve


# In[2]:

TARGET_COL = 'flag'
DROP_COLS = 'imei'


# # Data Preprocess

# In[3]:

INPUT_PATH_1 = '/Users/gaoyan/Documents/Data/Mstaz/170107_baseline/dataset2/formated-trainset'
INPUT_PATH_2 = '/Users/gaoyan/Documents/Data/Mstaz/170107_baseline/dataset2/formated-testset'
data1_train = pd.read_csv(INPUT_PATH_1)
data1_test = pd.read_csv(INPUT_PATH_2)


# In[4]:

imei = data1_test['imei']


# In[5]:

data1_train.drop([DROP_COLS], axis = 1, inplace = True)
data1_test.drop([DROP_COLS], axis = 1,inplace = True)


# In[6]:

data1_train.fillna(0, inplace = True)
data1_test.fillna(0, inplace = True)


# # Data Summary

# In[7]:

print (data1_train.shape)
print (data1_test.shape)
print (data1_train[data1_train[TARGET_COL] == 1].shape)
print (data1_train[data1_train[TARGET_COL] == 0].shape)
print (data1_test[data1_test[TARGET_COL] == 1].shape)
print (data1_test[data1_test[TARGET_COL] == 0].shape)


# In[8]:

y_train = data1_train[TARGET_COL]
X_train = data1_train.drop([TARGET_COL], axis = 1)
X_train, X_validate, y_train, y_validate = train_test_split(X_train, y_train, test_size=0.3, random_state=0)
y_test = data1_test[TARGET_COL]
X_test = data1_test.drop([TARGET_COL], axis = 1)
print (X_train.shape)
print (y_train.shape)
print (X_validate.shape)
print (y_validate.shape)
print (X_test.shape)
print (y_test.shape)


# In[9]:

feature_cols = X_train.columns# return index label
print ("\nFeature columns:\n{}".format(feature_cols))


# In[10]:

def positive_rate(y):
    n_ones = np.size(y[y == 1])
    #print ('\nNumber of label 1:', n_ones)
    # Calculate label zero
    n_zeros = np.size(y[y == 0])
    #print ('Number of label 0:', n_zeros)
    # Calculate pos rate
    ones_rate = float(n_ones) / float(n_ones + n_zeros)
    return ones_rate
print ('train')
print (positive_rate(y_train))
print ('validate')
print (positive_rate(y_validate))
print ('test')
print (positive_rate(y_test))


# In[11]:

# Tuned model:
from sklearn.ensemble import GradientBoostingClassifier


# In[12]:

clf_gbt = GradientBoostingClassifier(n_estimators = 50, max_depth = 18, min_samples_split = 20, max_leaf_nodes = 50, random_state = 0 )
clf_gbt.fit(X_train, y_train)
# Use default threshhold: 0.5
print ('threshold p=0.5')
# Validation Dataset
y_pred_validate = clf_gbt.predict(X_validate)
print ('Validation DataSet')
print (confusion_matrix(y_validate, y_pred_validate)) # in confusion matrix, first col is 0, second col is 1
print (accuracy_score(y_validate, y_pred_validate))
print (f1_score(y_validate, y_pred_validate))
# Test Dataset
print ('Test DataSet')
y_pred_test = clf_gbt.predict(X_test)
print (confusion_matrix(y_test, y_pred_test))
print (accuracy_score(y_test, y_pred_test))
print (f1_score(y_test, y_pred_test))


# In[13]:

# Use default threshhold: 0.4
THRESHOLD = 0.4
print ('threshold p=0.4')
print ('Test DataSet')
y_prob_test = clf_gbt.predict_proba(X_test)[:,1]
y_pred_test2 = [1 if y_prob_test[i]> THRESHOLD else 0 for i in range(0, len(y_prob_test))]
print (confusion_matrix(y_test, y_pred_test2))
print (accuracy_score(y_test, y_pred_test2))
print (f1_score(y_test, y_pred_test2))


# In[14]:

# plot using default classifier
# ROC curve
y_prob_test = clf_gbt.predict_proba(X_test)
rate = roc_curve(y_test,y_prob_test[:,1])
fpr = rate[0]
tpr = rate[1]
plt.plot(fpr, tpr, color='darkorange')
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC curve')
plt.show()

# precision Recall Curve
score = precision_recall_curve(y_test,y_prob_test[:,1])
precision = score[0]
recall = score[1]

plt.plot(recall, precision, color='darkorange')
plt.ylim([0.0, 1.05])
plt.xlim([0.0, 1.0])
plt.xlabel('Recall')
plt.ylabel('Precision')
plt.title('Precision_Recall Curve')
plt.show()


# In[15]:

ground_truth = pd.concat([imei, y_test], axis = 1)
gbt_prob = pd.concat([imei, pd.DataFrame(y_prob_test[:,1], columns = ['gbt_prob_v2'])], axis = 1)


# In[16]:

ground_truth
ground_truth.to_csv('ground_truth.csv', index = False)


# In[18]:

gbt_prob
gbt_prob.to_csv('gbt_prob_v2_second.csv', index = False)


# In[19]:

# Code for grid search


# In[13]:

N_ESTIMATORS_L = [20, 50]
MAX_DEPTH_L = [6,12,18]
MIN_SAMPLES_SPLIT_L= [20,50]
MAX_LEAF_NODES_L = [50,100]
max_score = 0
param = [0,0,0,0]
for i in N_ESTIMATORS_L:
    for j in MAX_DEPTH_L:
        for k in MIN_SAMPLES_SPLIT_L:
            for l in MAX_LEAF_NODES_L :
                clf = GradientBoostingClassifier(n_estimators = i, max_depth = j, min_samples_split = k, max_leaf_nodes = l, random_state = 0 )
                clf.fit(X_train, y_train)
                y_pred_validate = clf.predict(X_validate) 
                score = f1_score(y_validate, y_pred_validate)
                print ([i, j , k, l])
                print (score)
                if score > max_score:
                    max_score = score
                    param = [i, j, k, l] 
print (param)
print (max_score)

