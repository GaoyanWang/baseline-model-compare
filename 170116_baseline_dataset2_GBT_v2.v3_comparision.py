
# coding: utf-8

# In[15]:

#import standard modules
import numpy as np
import pandas as pd
get_ipython().magic('matplotlib inline')
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.cross_validation import train_test_split
from sklearn.metrics import roc_curve
import matplotlib.pyplot as plt
from sklearn.metrics import precision_recall_curve


# In[16]:

ground_truth = pd.read_csv('ground_truth.csv')
gst_prob_v2 = pd.read_csv('gbt_prob_v2.csv')
gbt_prob_v3 = pd.read_csv('gbt_prob_v3.csv')


# In[17]:

total_prob =ground_truth
total_prob['gbt_prob_v2'] = gst_prob_v2['gbt_prob_v2']
total_prob['gbt_prob_v3'] = gbt_prob_v3['gbt_prob_v3']


# In[18]:

total_prob


# In[19]:

# precision Recall Curve
score_gbt_v2 = precision_recall_curve(total_prob['flag'], total_prob['gbt_prob_v2'])
precision_gbt_v2 = score_gbt_v2[0]
recall_gbt_v2 = score_gbt_v2[1]

score_gbt_v3 = precision_recall_curve(total_prob['flag'], total_prob['gbt_prob_v3'])
precision_gbt_v3 = score_gbt_v3[0]
recall_gbt_v3 = score_gbt_v3[1]

plt.plot(recall_gbt_v2, precision_gbt_v2,color='darkorange')
plt.plot(recall_gbt_v3, precision_gbt_v3, color= 'green')

plt.legend(['gbt_v2', 'gbt_v3_lixin'])

plt.ylim([0.0, 1.05])
plt.xlim([0.0, 1.0])
plt.xlabel('Recall')
plt.ylabel('Precision')
plt.title('Precision_Recall Curve')
plt.show()


# In[ ]:




# In[ ]:




# In[ ]:



